use conf::Config;
use sas_common::config::*;
use sas_common::servers::pre_store::PreStoreServer;
use sas_common::servers::*;

// This program reads from the pre-processed server and sends them to
// processed storage.

fn main() {
    // Storage processor reads from pre-processed storage and interprets
    // the info into logical objects. They are then sent to the processed
    // storage.
    let settings = Config::new().with_env_prefix("APP");

    println!("Test Env dump");
    println!(
        "Bind : {}",
        std::env::var("APP_BIND_ADDR").unwrap_or("[Nothing]".to_string())
    );
    println!(
        "DB : {}",
        std::env::var("APP_DB_ADDR").unwrap_or("[Nothing]".to_string())
    );

    let server_str = settings
        .get("BIND_ADDR")
        .unwrap_or("localhost:4545".to_string());
    let db_str = settings
        .get("DB_ADDR")
        .unwrap_or("mysql://root:abcd1234@localhost:3306/csv_data".to_string());

    // Start a retry loop
    let mut server: PreStoreServer = loop {
        let connect_attempt = pre_store::PreStoreServer::connect(&server_str, &db_str);

        if connect_attempt.is_ok() {
            break connect_attempt.unwrap();
        } else {
            println!("Error connecting to PreStoreServer. Retrying in 5 seconds.");

            std::thread::sleep(std::time::Duration::from_secs(5));
        }
    };

    println!("Started server on {}.", server_str);

    server.run();
}
